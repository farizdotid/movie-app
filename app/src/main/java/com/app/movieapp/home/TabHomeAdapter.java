package com.app.movieapp.home;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.app.movieapp.nowplaying.NowPlayingFragment;
import com.app.movieapp.popular.PopularFragment;
import com.app.movieapp.upcoming.UpcomingFragment;


/**
 * Created by Fariz Ramadhan.
 * website : https://farizdotid.com/
 * github : https://github.com/farizdotid
 * linkedin : https://www.linkedin.com/in/farizramadhan/
 */
public class TabHomeAdapter extends FragmentStatePagerAdapter {

    private int mNumOfTabs;

    public TabHomeAdapter(FragmentManager fm, int mNumOfTabs) {
        super(fm);
        this.mNumOfTabs = mNumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new NowPlayingFragment();
            case 1:
                return new PopularFragment();
            case 2:
                return new UpcomingFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}