package com.app.movieapp.moviedetail;

import com.app.movieapp.model.actor.ResponseActor;
import com.app.movieapp.model.moviesdetail.ResponseMoviesDetail;
import com.app.movieapp.utils.apiservice.BaseApiService;
import com.app.movieapp.utils.apiservice.UtilsApi;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiFunction;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Fariz Ramadhan.
 * website : https://farizdotid.com/
 * github : https://github.com/farizdotid
 * linkedin : https://www.linkedin.com/in/farizramadhan/
 */
public class DetailPresenter implements DetailContract.Presenter {

    private DetailContract.View mView;
    private BaseApiService mApiService;

    public DetailPresenter(DetailContract.View mView) {
        this.mView = mView;
        this.mApiService = UtilsApi.getAPIService();
    }

    @Override
    public void requestDetail(int id, String apiKey) {
        mView.showLoading();
        Observable.zip(reqMovieDetail(id, apiKey), reqCast(id, apiKey),
                new BiFunction<ResponseMoviesDetail, ResponseActor, Detail>() {
                    @Override
                    public Detail apply(ResponseMoviesDetail responseMoviesDetail, ResponseActor responseActor) throws Exception {
                        return new Detail(responseMoviesDetail, responseActor);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Detail>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Detail detail) {
                        mView.isReqDetail(detail);
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.dismissLoading();
                        mView.message(e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        mView.dismissLoading();
                    }
                });
    }

    private Observable<ResponseMoviesDetail> reqMovieDetail(int id, String apiKey){
        return mApiService.requestMovieDetail(id, apiKey, "videos");
    }

    private Observable<ResponseActor> reqCast(int id, String apiKey){
        return mApiService.requestActor(id, apiKey);
    }
}