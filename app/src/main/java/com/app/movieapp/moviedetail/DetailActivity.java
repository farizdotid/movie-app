package com.app.movieapp.moviedetail;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.movieapp.R;
import com.app.movieapp.model.actor.CastItem;
import com.app.movieapp.model.moviesdetail.GenresItem;
import com.app.movieapp.utils.BaseAppActivity;
import com.app.movieapp.utils.Constant;
import com.app.movieapp.utils.DelayedProgressDialog;
import com.app.movieapp.utils.FunctionHelper;
import com.app.movieapp.utils.apiservice.UtilsApi;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class DetailActivity extends BaseAppActivity implements DetailContract.View {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_layout)
    CollapsingToolbarLayout toolbarLayout;
    @BindView(R.id.app_bar)
    AppBarLayout appBar;
    @BindView(R.id.fabPlay)
    FloatingActionButton fabPlay;
    @BindView(R.id.ivPoster)
    ImageView ivPoster;
    @BindView(R.id.ivImage)
    ImageView ivImage;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvCategory)
    TextView tvCategory;
    @BindView(R.id.tvRating)
    TextView tvRating;
    @BindView(R.id.tvMinutes)
    TextView tvMinutes;
    @BindView(R.id.tvDate)
    TextView tvDate;
    @BindView(R.id.tvLanguange)
    TextView tvLanguange;
    @BindView(R.id.tvSynopsis)
    TextView tvSynopsis;
    @BindView(R.id.rvActor)
    RecyclerView rvActor;

    private Unbinder unbinder;
    private DetailPresenter detailPresenter;
    private DelayedProgressDialog mProgressDialog;
    private String mIdYoutube;
    private CastAdapter castAdapter;

    private List<CastItem> castItems = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        unbinder = ButterKnife.bind(this);
        detailPresenter = new DetailPresenter(this);
        mProgressDialog = new DelayedProgressDialog();

        int id = getIntent().getExtras().getInt(Constant.ARGS_ID_MOVIE);

        detailPresenter.requestDetail(id, UtilsApi.getApiKey());

        castAdapter = new CastAdapter(this, castItems, null);
        rvActor.setLayoutManager(new LinearLayoutManager(this));
        rvActor.setItemAnimator(new DefaultItemAnimator());
        rvActor.setAdapter(castAdapter);

        fabPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FunctionHelper.watchYoutubeVideo(DetailActivity.this, mIdYoutube);
            }
        });

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void showLoading() {
        mProgressDialog.show(getSupportFragmentManager(), getString(R.string.tag_loading));
    }

    @Override
    public void dismissLoading() {
        mProgressDialog.cancel();
    }

    @Override
    public void message(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void isReqDetail(Detail detail) {
        String backdropPath = detail.getResponseMoviesDetail().getBackdropPath();
        String posterPath = detail.getResponseMoviesDetail().getPosterPath();
        String title = detail.getResponseMoviesDetail().getTitle();
        List<GenresItem> genresItems = detail.getResponseMoviesDetail().getGenres();
        Double voteAverage = detail.getResponseMoviesDetail().getVoteAverage();
        int voteCount = detail.getResponseMoviesDetail().getVoteCount();
        int runtime = detail.getResponseMoviesDetail().getRuntime();
        String releaseDate = detail.getResponseMoviesDetail().getReleaseDate();
        String originalLanguange = detail.getResponseMoviesDetail().getOriginalLanguage();
        String overview = detail.getResponseMoviesDetail().getOverview();
        String idYoutube = detail.getResponseMoviesDetail().getVideos().getResults().get(0).getKey();

        Glide.with(this)
                .load(UtilsApi.getBaseUrlImage() + backdropPath)
                .apply(new RequestOptions().centerCrop())
                .into(ivPoster);

        Glide.with(this)
                .load(UtilsApi.getBaseUrlImage() + posterPath)
                .apply(new RequestOptions().centerCrop().transform(new RoundedCorners(8)))
                .into(ivImage);

        mIdYoutube = idYoutube;

        tvTitle.setText(title);
        List<String> genres = new ArrayList<>();
        for (int i = 0; i < genresItems.size(); i++) {
            String name = genresItems.get(i).getName();
            genres.add(name);
        }
        tvCategory.setText(genres.toString().substring(1, genres.toString().length() - 1));

        tvRating.setText("IMDB " + voteAverage + " (" + voteCount + " votes)");
        tvMinutes.setText(runtime + " Minutes");

        try {
            @SuppressLint("SimpleDateFormat") DateFormat dateFormatNow = new SimpleDateFormat("yyyy-MM-dd");
            Date dateConvert = dateFormatNow.parse(releaseDate);

            @SuppressLint("SimpleDateFormat") SimpleDateFormat outputFormatStarts = new SimpleDateFormat("dd MMMM yyyy");
            String formattedDate = outputFormatStarts.format(dateConvert);

            tvDate.setText(formattedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        tvLanguange.setText(originalLanguange);
        tvSynopsis.setText(overview);

        castItems = detail.getResponseActor().getCast();
        if (castItems.size() != 0){
            castAdapter = new CastAdapter(this, castItems, null);
            rvActor.setAdapter(castAdapter);
            castAdapter.notifyDataSetChanged();
        }
    }
}