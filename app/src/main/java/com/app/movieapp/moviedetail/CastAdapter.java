package com.app.movieapp.moviedetail;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.movieapp.R;
import com.app.movieapp.model.actor.CastItem;
import com.app.movieapp.utils.apiservice.UtilsApi;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Fariz Ramadhan.
 * website : https://farizdotid.com/
 * github : https://github.com/farizdotid
 * linkedin : https://www.linkedin.com/in/farizramadhan/
 */
public class CastAdapter extends
        RecyclerView.Adapter<CastAdapter.ViewHolder> {

    private static final String TAG = CastAdapter.class.getSimpleName();

    private Context context;
    private List<CastItem> list;
    private CastAdapterCallback mAdapterCallback;

    public CastAdapter(Context context, List<CastItem> list, CastAdapterCallback adapterCallback) {
        this.context = context;
        this.list = list;
        this.mAdapterCallback = adapterCallback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movie_cast,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        CastItem item = list.get(position);

        String profilePath = item.getProfilePath();
        String character = item.getCharacter();
        String name = item.getName();

        Glide.with(context)
                .load(UtilsApi.getBaseUrlImage() + profilePath)
                .apply(RequestOptions.circleCropTransform())
                .into(holder.ivPhoto);

        holder.tvCharacter.setText(character);
        holder.tvName.setText(name);
    }

    @Override
    public int getItemCount() {
        int limit = 5;
        if (list.size() > limit) {
            return limit;
        } else {
            return list.size();
        }
    }

    public void clear() {
        int size = this.list.size();
        this.list.clear();
        notifyItemRangeRemoved(0, size);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ivPhoto)
        ImageView ivPhoto;
        @BindView(R.id.tvCharacter)
        TextView tvCharacter;
        @BindView(R.id.tvName)
        TextView tvName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface CastAdapterCallback {
        void onRowCastAdapterClicked(int position);
    }
}