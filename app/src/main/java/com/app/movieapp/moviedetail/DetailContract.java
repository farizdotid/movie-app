package com.app.movieapp.moviedetail;

/**
 * Created by Fariz Ramadhan.
 * website : https://farizdotid.com/
 * github : https://github.com/farizdotid
 * linkedin : https://www.linkedin.com/in/farizramadhan/
 */
public interface DetailContract {
    interface View {
        void showLoading();
        void dismissLoading();
        void message(String message);
        void isReqDetail(Detail detail);
    }

    interface Presenter {
        void requestDetail(int id, String apiKey);
    }
}
