package com.app.movieapp.moviedetail;

import com.app.movieapp.model.actor.ResponseActor;
import com.app.movieapp.model.moviesdetail.ResponseMoviesDetail;

/**
 * Created by Fariz Ramadhan.
 * website : https://farizdotid.com/
 * github : https://github.com/farizdotid
 * linkedin : https://www.linkedin.com/in/farizramadhan/
 */
public class Detail {
    private ResponseMoviesDetail responseMoviesDetail;
    private ResponseActor responseActor;

    public Detail(ResponseMoviesDetail responseMoviesDetail, ResponseActor responseActor) {
        this.responseMoviesDetail = responseMoviesDetail;
        this.responseActor = responseActor;
    }

    public ResponseMoviesDetail getResponseMoviesDetail() {
        return responseMoviesDetail;
    }

    public void setResponseMoviesDetail(ResponseMoviesDetail responseMoviesDetail) {
        this.responseMoviesDetail = responseMoviesDetail;
    }

    public ResponseActor getResponseActor() {
        return responseActor;
    }

    public void setResponseActor(ResponseActor responseActor) {
        this.responseActor = responseActor;
    }
}
