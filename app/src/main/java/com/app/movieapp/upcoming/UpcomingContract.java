package com.app.movieapp.upcoming;

import com.app.movieapp.model.movies.ResponseMovies;

/**
 * Created by Fariz Ramadhan.
 * website : https://farizdotid.com/
 * github : https://github.com/farizdotid
 * linkedin : https://www.linkedin.com/in/farizramadhan/
 */
public interface UpcomingContract {
    interface View {
        void showLoading();
        void dismissLoading();
        void message(String message);
        void isReqUpcoming(ResponseMovies responseMovies);
    }

    interface Presenter {
        void requestMovieUpcoming(String apiKey, int page);
    }
}
