package com.app.movieapp.upcoming;

import com.app.movieapp.model.movies.ResponseMovies;
import com.app.movieapp.utils.apiservice.BaseApiService;
import com.app.movieapp.utils.apiservice.UtilsApi;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Fariz Ramadhan.
 * website : https://farizdotid.com/
 * github : https://github.com/farizdotid
 * linkedin : https://www.linkedin.com/in/farizramadhan/
 */
public class UpcomingPresenter implements UpcomingContract.Presenter {

    private UpcomingContract.View mView;
    private BaseApiService mApiService;

    public UpcomingPresenter(UpcomingContract.View mView) {
        this.mView = mView;
        this.mApiService = UtilsApi.getAPIService();
    }


    @Override
    public void requestMovieUpcoming(String apiKey, int page) {
        mView.showLoading();
        mApiService.requestMovieUpcoming(apiKey, page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseMovies>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ResponseMovies responseMovies) {
                        mView.isReqUpcoming(responseMovies);
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.dismissLoading();
                        mView.message(e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        mView.dismissLoading();
                    }
                });
    }
}