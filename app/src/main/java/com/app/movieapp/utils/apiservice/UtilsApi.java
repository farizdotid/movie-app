package com.app.movieapp.utils.apiservice;

/**
 * Created by Fariz Ramadhan.
 * website : https://farizdotid.com/
 * github : https://github.com/farizdotid
 * linkedin : https://www.linkedin.com/in/farizramadhan/
 */
public class UtilsApi {

    private static final String BASE_URL_API = "https://api.themoviedb.org/3/";
    private static final String BASE_URL_IMAGE = "https://image.tmdb.org/t/p/w500";
    private static final String API_KEY = "8724b892dfcdaadab88f5d105712dcb5";

    public static BaseApiService getAPIService(){
        return RetrofitClient.getClient(BASE_URL_API).create(BaseApiService.class);
    }

    public static String getBaseUrlImage(){
        return BASE_URL_IMAGE;
    }

    public static String getApiKey() {
        return API_KEY;
    }
}
