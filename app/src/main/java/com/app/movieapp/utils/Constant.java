package com.app.movieapp.utils;

/**
 * Created by Fariz Ramadhan.
 * website : https://farizdotid.com/
 * github : https://github.com/farizdotid
 * linkedin : https://www.linkedin.com/in/farizramadhan/
 */
public class Constant {
    public static final String ARGS_ID_MOVIE = "args_id_movie";
}
