package com.app.movieapp.utils.apiservice;

import com.app.movieapp.model.actor.ResponseActor;
import com.app.movieapp.model.movies.ResponseMovies;
import com.app.movieapp.model.moviesdetail.ResponseMoviesDetail;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Fariz Ramadhan.
 * website : https://farizdotid.com/
 * github : https://github.com/farizdotid
 * linkedin : https://www.linkedin.com/in/farizramadhan/
 */
public interface BaseApiService {

    @GET("movie/now_playing")
    Observable<ResponseMovies> requestMovieNowPlaying(
            @Query("api_key") String apiKey,
            @Query("page") int page
    );

    @GET("movie/popular")
    Observable<ResponseMovies> requestMoviePopular(
            @Query("api_key") String apiKey,
            @Query("page") int page
    );

    @GET("movie/upcoming")
    Observable<ResponseMovies> requestMovieUpcoming(
            @Query("api_key") String apiKey,
            @Query("page") int page
    );

    @GET("movie/{id}")
    Observable<ResponseMoviesDetail> requestMovieDetail(
            @Path("id") int id,
            @Query("api_key") String apiKey,
            @Query("append_to_response") String appendToResponse
    );

    @GET("movie/{id}/credits")
    Observable<ResponseActor> requestActor(
            @Path("id") int id,
            @Query("api_key") String apiKey
    );

    @GET("search/movie")
    Single<ResponseMovies> requestSearchMovie(
            @Query("api_key") String apiKey,
            @Query("query") String keyword
    );
}
