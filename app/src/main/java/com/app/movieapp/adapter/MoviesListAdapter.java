package com.app.movieapp.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.movieapp.R;
import com.app.movieapp.model.movies.Movie;
import com.app.movieapp.model.movies.ResultsItem;
import com.app.movieapp.utils.apiservice.UtilsApi;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Fariz Ramadhan.
 * website : https://farizdotid.com/
 * github : https://github.com/farizdotid
 * linkedin : https://www.linkedin.com/in/farizramadhan/
 */
public class MoviesListAdapter extends
        RecyclerView.Adapter<MoviesListAdapter.ViewHolder> {

    private static final String TAG = MoviesListAdapter.class.getSimpleName();

    private Context context;
    private List<Movie> list;
    private MoviesListAdapterCallback mAdapterCallback;

    public MoviesListAdapter(Context context, List<Movie> list, MoviesListAdapterCallback adapterCallback) {
        this.context = context;
        this.list = list;
        this.mAdapterCallback = adapterCallback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movie_list,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Movie item = list.get(position);

        Double voteAverage = item.getVoteAverage();
        String title = item.getTitle();
        String posterPath = item.getPosterPath();
        String releaseDate = item.getReleaseDate();

        Glide.with(context)
                .load(UtilsApi.getBaseUrlImage() + posterPath)
                .apply(new RequestOptions().centerCrop())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        holder.pbLoading.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.pbLoading.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(holder.ivImage);

        holder.tvRating.setText(String.valueOf(voteAverage));
        holder.tvTitle.setText(title);

        try {
            @SuppressLint("SimpleDateFormat") DateFormat dateFormatNow = new SimpleDateFormat("yyyy-MM-dd");
            Date dateConvert = dateFormatNow.parse(releaseDate);

            @SuppressLint("SimpleDateFormat") SimpleDateFormat outputFormatStarts = new SimpleDateFormat("dd MMMM yyyy");
            String formattedDate = outputFormatStarts.format(dateConvert);

            holder.tvDate.setText(formattedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void clear() {
        int size = this.list.size();
        this.list.clear();
        notifyItemRangeRemoved(0, size);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvRating)
        TextView tvRating;
        @BindView(R.id.tvDate)
        TextView tvDate;
        @BindView(R.id.pbLoading)
        ProgressBar pbLoading;
        @BindView(R.id.ivImage)
        ImageView ivImage;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mAdapterCallback.onRowMoviesListAdapterClicked(getAdapterPosition());
                }
            });
        }
    }

    public interface MoviesListAdapterCallback {
        void onRowMoviesListAdapterClicked(int position);
    }
}