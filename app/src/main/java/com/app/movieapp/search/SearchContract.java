package com.app.movieapp.search;

import com.app.movieapp.model.movies.ResponseMovies;

/**
 * Created by Fariz Ramadhan.
 * website : https://farizdotid.com/
 * github : https://github.com/farizdotid
 * linkedin : https://www.linkedin.com/in/farizramadhan/
 */
public interface SearchContract {
    interface View {
        void showLoading();
        void dismissLoading();
        void message(String message);
        void isReqSearch(ResponseMovies responseMovies);
    }

    interface Presenter {
        void requestSearch(String apiKey, String keyword);
    }
}