package com.app.movieapp.search;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.app.movieapp.R;
import com.app.movieapp.adapter.MoviesGridAdapter;
import com.app.movieapp.model.movies.Movie;
import com.app.movieapp.model.movies.ResponseMovies;
import com.app.movieapp.model.movies.ResultsItem;
import com.app.movieapp.moviedetail.DetailActivity;
import com.app.movieapp.utils.BaseAppActivity;
import com.app.movieapp.utils.Constant;
import com.app.movieapp.utils.apiservice.UtilsApi;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SearchActivity extends BaseAppActivity
        implements SearchContract.View, MoviesGridAdapter.MoviesAdapterCallback {

    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;
    @BindView(R.id.rvSearch)
    RecyclerView rvSearch;
    @BindView(R.id.etSearch)
    EditText etSearch;
    @BindView(R.id.ivBack)
    ImageView ivBack;

    private Unbinder unbinder;
    private SearchPresenter searchPresenter;
    private MoviesGridAdapter moviesGridAdapter;

    private List<Movie> movieList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        unbinder = ButterKnife.bind(this);
        searchPresenter = new SearchPresenter(this);

        moviesGridAdapter = new MoviesGridAdapter(this, movieList, this);
        rvSearch.setLayoutManager(new GridLayoutManager(this, 2));
        rvSearch.setItemAnimator(new DefaultItemAnimator());
        rvSearch.setAdapter(moviesGridAdapter);

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int countChar = s.length();
                if (countChar > 3) {
                    String keyword = s.toString();
                    searchPresenter.requestSearch(UtilsApi.getApiKey(), keyword);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void showLoading() {
        pbLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissLoading() {
        pbLoading.setVisibility(View.GONE);
    }

    @Override
    public void message(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void isReqSearch(ResponseMovies responseMovies) {
        moviesGridAdapter.clear();
        List<ResultsItem> resultsItemList = responseMovies.getResults();
        if (resultsItemList.size() != 0) {
            for (int i = 0; i < resultsItemList.size(); i++) {
                int id = resultsItemList.get(i).getId();
                Double voteAverage = resultsItemList.get(i).getVoteAverage();
                String title = resultsItemList.get(i).getTitle();
                String posterPath = resultsItemList.get(i).getPosterPath();
                String releaseDate = resultsItemList.get(i).getReleaseDate();

                movieList.add(new Movie(id, voteAverage, title, posterPath, releaseDate));
            }

            moviesGridAdapter = new MoviesGridAdapter(this, movieList, this);
            rvSearch.setAdapter(moviesGridAdapter);
            moviesGridAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onRowMoviesAdapterClicked(int position) {
        int id = movieList.get(position).getId();
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra(Constant.ARGS_ID_MOVIE, id);
        startActivity(intent);
    }
}
