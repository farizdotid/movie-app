package com.app.movieapp.search;

import com.app.movieapp.model.movies.ResponseMovies;
import com.app.movieapp.utils.apiservice.BaseApiService;
import com.app.movieapp.utils.apiservice.UtilsApi;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Fariz Ramadhan.
 * website : https://farizdotid.com/
 * github : https://github.com/farizdotid
 * linkedin : https://www.linkedin.com/in/farizramadhan/
 */
public class SearchPresenter implements SearchContract.Presenter {

    private SearchContract.View mView;
    private BaseApiService mApiService;

    public SearchPresenter(SearchContract.View mView) {
        this.mView = mView;
        this.mApiService = UtilsApi.getAPIService();
    }

    @Override
    public void requestSearch(String apiKey, String keyword) {
        mView.showLoading();
        mApiService.requestSearchMovie(apiKey, keyword)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<ResponseMovies>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(ResponseMovies responseMovies) {
                        mView.isReqSearch(responseMovies);
                        mView.dismissLoading();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.dismissLoading();
                        mView.message(e.getMessage());
                    }
                });
    }
}