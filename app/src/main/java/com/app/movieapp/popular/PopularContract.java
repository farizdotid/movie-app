package com.app.movieapp.popular;

import com.app.movieapp.model.movies.ResponseMovies;

/**
 * Created by Fariz Ramadhan.
 * website : https://farizdotid.com/
 * github : https://github.com/farizdotid
 * linkedin : https://www.linkedin.com/in/farizramadhan/
 */
public interface PopularContract {
    interface View {
        void showLoading();
        void dismissLoading();
        void message(String message);
        void isReqPopular(ResponseMovies responseMovies);
    }

    interface Presenter {
        void requestMoviePopular(String apiKey, int page);
    }
}
