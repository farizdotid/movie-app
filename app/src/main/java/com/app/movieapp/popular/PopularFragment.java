package com.app.movieapp.popular;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.app.movieapp.R;
import com.app.movieapp.adapter.MoviesGridAdapter;
import com.app.movieapp.adapter.MoviesListAdapter;
import com.app.movieapp.model.movies.Movie;
import com.app.movieapp.model.movies.ResponseMovies;
import com.app.movieapp.model.movies.ResultsItem;
import com.app.movieapp.moviedetail.DetailActivity;
import com.app.movieapp.utils.Constant;
import com.app.movieapp.utils.DelayedProgressDialog;
import com.app.movieapp.utils.EndlessRecyclerViewScrollListener;
import com.app.movieapp.utils.apiservice.UtilsApi;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class PopularFragment extends Fragment
        implements PopularContract.View, MoviesGridAdapter.MoviesAdapterCallback,
        MoviesListAdapter.MoviesListAdapterCallback {

    @BindView(R.id.rvMovies)
    RecyclerView rvMovies;
    @BindView(R.id.ivGrid)
    ImageView ivGrid;
    @BindView(R.id.ivList)
    ImageView ivList;

    private Unbinder unbinder;
    private DelayedProgressDialog mProgressDialog;
    private PopularPresenter popularPresenter;
    private MoviesGridAdapter moviesGridAdapter;
    private MoviesListAdapter moviesListAdapter;

    private List<Movie> movieList = new ArrayList<>();
    private int mTotalItemCount;
    private boolean isGrid;

    public PopularFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_popular, container, false);
        unbinder = ButterKnife.bind(this, view);
        mProgressDialog = new DelayedProgressDialog();
        popularPresenter = new PopularPresenter(this);

        initGridAdapter();
        moviesGridAdapter.clear();
        isGrid = true;

        popularPresenter.requestMoviePopular(UtilsApi.getApiKey(), 1);

        return view;
    }

    private void initListAdapter() {
        moviesListAdapter = new MoviesListAdapter(getActivity(), movieList, this);
        LinearLayoutManager linearLayout = new LinearLayoutManager(getActivity());
        rvMovies.setLayoutManager(linearLayout);
        EndlessRecyclerViewScrollListener scrollListener = new EndlessRecyclerViewScrollListener(linearLayout) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                mTotalItemCount = totalItemsCount;
                popularPresenter.requestMoviePopular(UtilsApi.getApiKey(), (page + 1));
            }
        };
        rvMovies.setItemAnimator(new DefaultItemAnimator());
        rvMovies.addOnScrollListener(scrollListener);
        rvMovies.setAdapter(moviesListAdapter);
        moviesListAdapter.notifyDataSetChanged();
    }

    private void initGridAdapter() {
        moviesGridAdapter = new MoviesGridAdapter(getActivity(), movieList, this);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        rvMovies.setLayoutManager(gridLayoutManager);
        EndlessRecyclerViewScrollListener scrollListener = new EndlessRecyclerViewScrollListener(gridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                mTotalItemCount = totalItemsCount;
                popularPresenter.requestMoviePopular(UtilsApi.getApiKey(), (page + 1));
            }
        };
        rvMovies.setItemAnimator(new DefaultItemAnimator());
        rvMovies.addOnScrollListener(scrollListener);
        rvMovies.setAdapter(moviesGridAdapter);
        moviesGridAdapter.notifyDataSetChanged();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ivGrid.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark),
                android.graphics.PorterDuff.Mode.SRC_IN);

        ivGrid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isGrid = true;
                initGridAdapter();

                ivGrid.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark),
                        android.graphics.PorterDuff.Mode.SRC_IN);

                ivList.setColorFilter(ContextCompat.getColor(getActivity(), R.color.grey),
                        android.graphics.PorterDuff.Mode.SRC_IN);
            }
        });

        ivList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isGrid = false;
                initListAdapter();

                ivList.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark),
                        android.graphics.PorterDuff.Mode.SRC_IN);

                ivGrid.setColorFilter(ContextCompat.getColor(getActivity(), R.color.grey),
                        android.graphics.PorterDuff.Mode.SRC_IN);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showLoading() {
        mProgressDialog.show(getChildFragmentManager(), getString(R.string.tag_loading));
    }

    @Override
    public void dismissLoading() {
        mProgressDialog.cancel();
    }

    @Override
    public void message(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void isReqPopular(ResponseMovies responseMovies) {
        List<ResultsItem> resultsItemList = responseMovies.getResults();
        if (resultsItemList.size() != 0) {
            for (int i = 0; i < resultsItemList.size(); i++) {
                int id = resultsItemList.get(i).getId();
                Double voteAverage = resultsItemList.get(i).getVoteAverage();
                String title = resultsItemList.get(i).getTitle();
                String posterPath = resultsItemList.get(i).getPosterPath();
                String releaseDate = resultsItemList.get(i).getReleaseDate();

                movieList.add(new Movie(id, voteAverage, title, posterPath, releaseDate));
            }

            if (isGrid){
                moviesGridAdapter = new MoviesGridAdapter(getActivity(), movieList, this);
                rvMovies.setAdapter(moviesGridAdapter);
                moviesGridAdapter.notifyDataSetChanged();
                rvMovies.scrollToPosition((mTotalItemCount - 12));
            } else {
                moviesListAdapter = new MoviesListAdapter(getActivity(), movieList, this);
                rvMovies.setAdapter(moviesListAdapter);
                moviesListAdapter.notifyDataSetChanged();
                rvMovies.scrollToPosition((mTotalItemCount - 12));
            }
        }
    }

    @Override
    public void onRowMoviesAdapterClicked(int position) {
        int id = movieList.get(position).getId();
        Intent intent = new Intent(getActivity(), DetailActivity.class);
        intent.putExtra(Constant.ARGS_ID_MOVIE, id);
        startActivity(intent);
    }

    @Override
    public void onRowMoviesListAdapterClicked(int position) {
        int id = movieList.get(position).getId();
        Intent intent = new Intent(getActivity(), DetailActivity.class);
        intent.putExtra(Constant.ARGS_ID_MOVIE, id);
        startActivity(intent);
    }
}
