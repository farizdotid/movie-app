package com.app.movieapp.nowplaying;

import com.app.movieapp.model.movies.ResponseMovies;
import com.app.movieapp.utils.apiservice.BaseApiService;
import com.app.movieapp.utils.apiservice.UtilsApi;

import io.reactivex.Observer;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Fariz Ramadhan.
 * website : https://farizdotid.com/
 * github : https://github.com/farizdotid
 * linkedin : https://www.linkedin.com/in/farizramadhan/
 */
public class NowPlayingPresenter implements NowPlayingContract.Presenter {

    private NowPlayingContract.View mView;
    private BaseApiService mApiService;

    public NowPlayingPresenter(NowPlayingContract.View mView) {
        this.mView = mView;
        this.mApiService = UtilsApi.getAPIService();
    }

    @Override
    public void requestMovieNowPlaying(String apiKey, int page) {
        mView.showLoading();
        mApiService.requestMovieNowPlaying(apiKey, page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseMovies>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ResponseMovies responseMovies) {
                        mView.isReqNowPlaying(responseMovies);
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.dismissLoading();
                        mView.message(e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        mView.dismissLoading();
                    }
                });
    }
}