package com.app.movieapp.nowplaying;

import com.app.movieapp.model.movies.ResponseMovies;

import java.util.List;

/**
 * Created by Fariz Ramadhan.
 * website : https://farizdotid.com/
 * github : https://github.com/farizdotid
 * linkedin : https://www.linkedin.com/in/farizramadhan/
 */
public interface NowPlayingContract {

    interface View {
        void showLoading();
        void dismissLoading();
        void message(String message);
        void isReqNowPlaying(ResponseMovies responseMovies);
    }

    interface Presenter {
        void requestMovieNowPlaying(String apiKey, int page);
    }
}