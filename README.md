# Movie App

References :
* https://www.uplabs.com/posts/movie-app-quick-buy ( UI )
* https://www.uplabs.com/posts/movie-booking-app-ui ( UI )
* https://www.uplabs.com/posts/movies-ticket-booking-icons ( UI )
* https://www.themoviedb.org/ ( DATA )

### Changelog
#### 17-03-2019 | 1.0.0
* Setup project :tada:
* Home layout
* Home Function
* Now Playing layout
* Now Playing function
* Popular layout
* Popular function
* Upcoming layout
* Upcoming function
* Layout detail
* Function detail
* Layout Search
* Function Search
* Add Icon App